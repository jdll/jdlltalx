const https = require("https");
const url = require("url");
const querystring = require("querystring");
let env = require("dotenv").config();
const hostname = env.parsed.PRETALX_HOSTNAME;
const apiKey = env.parsed.PRETALX_API_KEY;
const event = env.parsed.PRETALX_EVENT_ID;

const basePath = `/api/events/${event}/`;

const options = {
  hostname: hostname,
  port: 443,
  path: basePath,
  headers: {
    Authorization: `Token ${apiKey}`,
  },
};

const pretalxData = (endpoint) => {
  if (endpoint !== undefined) {
    options.path = basePath + endpoint;
  } else {
    options.path = basePath;
  }

  let dataUrl = url.parse(`https://${options.path}`, true);

  if (
    dataUrl.pathname === `${basePath}talks/` ||
    dataUrl.pathname === `${basePath}submissions/`
  ) {
    dataUrl.query["questions"] = "all";
    let query = querystring.encode(dataUrl.query);
    options.path = `${dataUrl.pathname}?${query}`;
  }

  return new Promise((resolve, reject) => {
    https
      .get(options, (res) => {
        let result = "";

        res.on("data", (d) => {
          result += d;
        });
        res.on("end", () => {
          return resolve(result);
        });
      })
      .on("error", (e) => {
        return reject(e);
      });
  });
};

module.exports = pretalxData;
