const { CONF_ID, ATELIER_ID, GONE_ID, STAND_ID, Q_BEGINNER_ID, Q_PUBLICS_ID } =
  require("dotenv").config().parsed;
const showdown = require("showdown");
const YAML = require("yaml");
const pretalxHandler = require("./pretalxHandler");

let converter = new showdown.Converter();

function isNumeric(value) {
  return /^-?\d+$/.test(value);
}

async function getSubmissions() {
  let result = new Array();
  let next = "submissions/";
  while (next != null) {
    let rawSubmissions = await pretalxHandler(next);
    let submissions = JSON.parse(rawSubmissions);
    result.push(...submissions.results);

    if (submissions.next === null) break;
    next = submissions.next.match(/submissions.*/g);
  }
  return result;
}

function cleanAbstract(abstract) {
  const regex = /[.?!…]$/g;
  let dotted = `${abstract}${abstract.match(regex) ? "" : "."}`;
  let unbreakable = makeUnbreakable(dotted);
  let clean = converter.makeHtml(unbreakable);
  return clean;
}

function cleanTitle(title) {
  return makeUnbreakable(title);
}

function makeUnbreakable(text) {
  return text
    .replaceAll(" !", "&nbsp;!")
    .replaceAll(" ?", "&nbsp;?")
    .replaceAll("« ", "«&nbsp;")
    .replaceAll(" »", "&nbsp;»")
    .replaceAll(" :", "&nbsp;:");
}

function getStandNumber(internal_notes) {
  let standNumber = 0;
  let clean;
  const regex = /(?<=---\r\n)(.*\r\n)*(?=\.\.\.)/g;
  if (internal_notes) {
    clean = internal_notes.match(regex);
  }
  if (clean) {
    let data = YAML.parse(clean[0]);
    standNumber = data.stand_number;
  }
  return standNumber;
}

function modelSubmission(submission) {
  let modeledSubmission = new Object();
  modeledSubmission.beginner = false;
  modeledSubmission.publics = "";
  submission.answers.forEach((answer) => {
    if (answer?.question?.id == Q_BEGINNER_ID && answer?.answer === "True") {
      modeledSubmission.beginner = true;
    }
    if (answer?.question?.id == Q_PUBLICS_ID) {
      modeledSubmission.publics = answer?.answer;
    }
  });
  modeledSubmission.type = submission.submission_type.fr;
  modeledSubmission.typeId = submission.submission_type_id;
  modeledSubmission.speakers = formatSpeakers(submission.speakers);
  modeledSubmission.room = submission.slot?.room?.fr;
  modeledSubmission.startTime = submission.slot?.start;
  modeledSubmission.duration = submission.duration;
  modeledSubmission.title = cleanTitle(submission.title);
  modeledSubmission.abstract = cleanAbstract(submission.abstract);
  if (modeledSubmission.typeId == STAND_ID) {
    modeledSubmission.standNumber = getStandNumber(submission.internal_notes);
  }
  return modeledSubmission;
}

function dispatchSubmission(modeledSubmission, data) {
  let date = new Date(modeledSubmission.startTime).getDay();

  if (date == 6 && modeledSubmission.typeId == CONF_ID) {
    data.samedi.confs.push(modeledSubmission);
  } else if (date == 6 && modeledSubmission.typeId == ATELIER_ID) {
    data.samedi.ateliers.push(modeledSubmission);
  } else if (date == 6 && modeledSubmission.typeId == GONE_ID) {
    data.samedi.gones.push(modeledSubmission);
  } else if (date == 0 && modeledSubmission.typeId == CONF_ID) {
    data.dimanche.confs.push(modeledSubmission);
  } else if (date == 0 && modeledSubmission.typeId == ATELIER_ID) {
    data.dimanche.ateliers.push(modeledSubmission);
  } else if (date == 0 && modeledSubmission.typeId == GONE_ID) {
    data.dimanche.gones.push(modeledSubmission);
  } else if (modeledSubmission.typeId == STAND_ID) {
    data.stands.push(modeledSubmission);
  }
}

function modelAndDispatchSubmissions(submissions, data) {
  for (let submission of submissions) {
    if (submission.state === "confirmed") {
      let modeledSubmission = modelSubmission(submission);
      dispatchSubmission(modeledSubmission, data);
    }
  }
  return data;
}

function sortByDate(submissionA, submissionB) {
  if (submissionA.startTime < submissionB.startTime) {
    return -1;
  }
  if (submissionA.startTime > submissionB.startTime) {
    return 1;
  }
  return 0;
}

function sortAscending(A, B) {
  if (A < B) {
    return -1;
  }
  if (A > B) {
    return 1;
  }
  return 0;
}

function sortByStandNumber(submissionA, submissionB) {
  if (
    !isNumeric(submissionA.standNumber) &&
    isNumeric(submissionB.standNumber)
  ) {
    return -1;
  }
  if (
    isNumeric(submissionA.standNumber) &&
    !isNumeric(submissionB.standNumber)
  ) {
    return 1;
  }
  return sortAscending(submissionA.standNumber, submissionB.standNumber);
}

function sortData(data) {
  data.samedi.confs.sort(sortByDate);
  data.samedi.ateliers.sort(sortByDate);
  data.samedi.gones.sort(sortByDate);
  data.dimanche.confs.sort(sortByDate);
  data.dimanche.ateliers.sort(sortByDate);
  data.dimanche.gones.sort(sortByDate);
  data.stands.sort(sortByStandNumber);
  return data;
}

function formatSpeakers(speakers) {
  let result = "";
  for (let speaker of speakers) {
    if (result != "") {
      result += ", ";
    }
    result += speaker.name;
  }
  return result;
}

let modeledData = async () => {
  let data = new Object();

  let rawEvent = await pretalxHandler();
  let event = JSON.parse(rawEvent);

  data.event = new Object();
  data.event.name = event.name.fr;
  data.event.date = new Object();
  data.event.date.start = new Date(event.date_from);
  data.event.date.end = new Date(event.date_to);

  data.samedi = new Object();
  data.samedi.confs = new Array();
  data.samedi.ateliers = new Array();
  data.samedi.gones = new Array();
  data.dimanche = new Object();
  data.dimanche.confs = new Array();
  data.dimanche.ateliers = new Array();
  data.dimanche.gones = new Array();

  data.stands = new Array();

  let submissions = await getSubmissions();
  let modeledData = modelAndDispatchSubmissions(submissions, data);
  return sortData(modeledData);
};

module.exports = modeledData;
